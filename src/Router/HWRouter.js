import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "../Home";
import About from "../About";
import Contact from "../Contact";
export default function HWRouter(){
    return (
        <BrowserRouter>
            <Routes>
                <Route path="" element={<Home />}></Route>
                <Route path="/about" element={<About />}></Route>
                <Route path="/contact" element={<Contact/>}></Route>
            </Routes>
        </BrowserRouter>
    );
}